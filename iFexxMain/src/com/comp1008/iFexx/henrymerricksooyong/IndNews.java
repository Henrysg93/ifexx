package com.comp1008.iFexx.henrymerricksooyong;

import com.example.ifexxmain.R;

import android.os.Bundle;
import android.app.Activity;
import android.view.Menu;

public class IndNews extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_ind_news);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.ind_news, menu);
		return true;
	}

}
