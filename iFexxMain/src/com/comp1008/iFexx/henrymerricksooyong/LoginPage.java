package com.comp1008.iFexx.henrymerricksooyong;


import com.example.ifexxmain.R;
import com.example.ifexxmain.R.layout;
import com.example.ifexxmain.R.menu;

import android.net.Uri;
import android.os.Bundle;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.view.Menu;
import android.content.Intent;
import android.os.AsyncTask;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
 


import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.os.StrictMode; 


public class LoginPage extends Activity implements OnClickListener {

	TextView result;
	Button ok, register;
	
	
	
	@SuppressLint("NewApi")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_login_page);
		
		// Login button clicked
        ok = (Button)findViewById(R.id.btn_signin);
        
        ok.setOnClickListener(this);
        register = (Button)findViewById(R.id.btn_register);
        register.setOnClickListener(this);
        
        result = (TextView)findViewById(R.id.lbl_result);
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build(); 
        StrictMode.setThreadPolicy(policy);

	}

	
	public void postLoginData() {
        // Create a new HttpClient and Post Header
        HttpClient httpclient = new DefaultHttpClient();
         
        /* login.php returns true if username and password is correct */
        HttpPost httppost = new HttpPost("http://www.ifexx.com/dologin.php");
 
        try {
            // Add user name and password
         EditText uname = (EditText)findViewById(R.id.txt_username);
         String username = uname.getText().toString();
 
         EditText pword = (EditText)findViewById(R.id.txt_password);
         String password = pword.getText().toString();
          
            List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(2);
            nameValuePairs.add(new BasicNameValuePair("username", username));
            nameValuePairs.add(new BasicNameValuePair("password", password));
            httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
 
            // Execute HTTP Post Request
            Log.w("IFEXX", "Execute HTTP Post Request");
            HttpResponse response = httpclient.execute(httppost);
             
            String str = inputStreamToString(response.getEntity().getContent()).toString();
            Log.w("IFEXX", str);
             
            if(str.toString().contains("Log-in Failed"))
            {
             Log.w("IFEXX", "FALSE");
             result.setText("Login unsuccessful");   
             
            }else
            {
             Log.w("IFEXX", "TRUE");
             result.setText("Login successful");   
             finish();
             Intent menu = new Intent(this, Menupage.class);
             
             startActivity(menu);
            }
 
        } catch (ClientProtocolException e) {
         e.printStackTrace();
        } catch (IOException e) {
         e.printStackTrace();
        }
    } 
   
	private StringBuilder inputStreamToString(InputStream is) {
	     String line = "";
	     StringBuilder total = new StringBuilder();
	     // Wrap a BufferedReader around the InputStream
	     BufferedReader rd = new BufferedReader(new InputStreamReader(is));
	     // Read response until the end
	     try {
	      while ((line = rd.readLine()) != null) { 
	        total.append(line); 
	      }
	     } catch (IOException e) {
	      e.printStackTrace();
	     }
	     // Return full string
	     return total;
	    }
	 
	    @Override
	    public void onClick(View view) {
	      if(view == ok){
	        postLoginData();
	      }
	        else if (view == register) {
	        	Uri uri = Uri.parse("http://www.ifexx.com/registeration.php");
		    	  Intent intent = new Intent(Intent.ACTION_VIEW, uri);
		    	  startActivity(intent);
	        }
	    }
	    
	
	    @Override
	    public void onBackPressed()
	    {

	       // super.onBackPressed(); // Comment this super call to avoid calling finish()
	    }


}
