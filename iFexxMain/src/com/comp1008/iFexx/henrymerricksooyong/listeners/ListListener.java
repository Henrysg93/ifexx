package com.comp1008.iFexx.henrymerricksooyong.listeners;

import java.util.List;

import com.comp1008.iFexx.henrymerricksooyong.IndSignal;
import com.comp1008.iFexx.henrymerricksooyong.data.RssItem;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;


public class ListListener implements OnItemClickListener {
	
	// List item's reference
	List<RssItem> listItems;
	// Calling activity reference
	Activity activity;
	
	public ListListener(List<RssItem> aListItems, Activity anActivity) {
		listItems = aListItems;
		activity  = anActivity;
	}
	
	/**
	 * Start a browser with url from the rss item.
	 */
	public void onItemClick(AdapterView<?> parent, View view, int pos, long id) {
		Uri uri;
	    String stringuri;
	    stringuri = (Uri.parse(listItems.get(pos).getLink())).toString();

	    Intent i = new Intent(activity, IndSignal.class);
	    i.putExtra("url", stringuri);

	    activity.startActivity(i); 

		
	}

}
