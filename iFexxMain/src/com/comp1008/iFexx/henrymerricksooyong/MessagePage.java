package com.comp1008.iFexx.henrymerricksooyong;

import com.example.ifexxmain.R;

import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.view.Menu;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;

public class MessagePage extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_message_page);
		
setupSignalslow();
		
	}

private void setupSignalslow() {
		ImageButton lowsig = (ImageButton) findViewById(R.id.footersignalsbutton);
		
		lowsig.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				startActivity(new Intent(MessagePage.this, AllSignals.class));
				
				
			}
		});
		
		setupMarketlow();
		
	}

	private void setupMarketlow() {
		ImageButton lowmarket = (ImageButton) findViewById(R.id.footermarketbutton);
		
		lowmarket.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				startActivity(new Intent(MessagePage.this, LiveQuotes.class));
				
			}
		});
		
		setupNewslow();
		
	}

	private void setupNewslow() {
		ImageButton lownews = (ImageButton) findViewById(R.id.footernewsbutton);
		
		lownews.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				startActivity(new Intent(MessagePage.this, AllNews.class));
				
			}
		});
		
		setupMenulow();
		
	}

	private void setupMenulow() {
		ImageButton lowmenu = (ImageButton) findViewById(R.id.footermenubutton);
		
		lowmenu.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				startActivity(new Intent(MessagePage.this, Menupage.class));
				
			}
		});
		
		setupBacklow();
		
	}

	private void setupBacklow() {
		ImageButton lowback = (ImageButton) findViewById(R.id.footerbackbutton);
		
		lowback.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				finish();
				
			}
		});
	
	
	   setupSendbutton();
}
	   
private void setupSendbutton() {
	   		ImageButton send = (ImageButton) findViewById(R.id.messagesendbutton);
	   		
	   		send.setOnClickListener(new View.OnClickListener() {
	   			
	   			final EditText name = (EditText) findViewById(R.id.txt_username);
	   		    final EditText address = (EditText) findViewById(R.id.txt_password);
	   		    final EditText message = (EditText) findViewById(R.id.editText3);
	   			
	   			@Override
	   			public void onClick(View v) {
	   				
	   				Intent email = new Intent(android.content.Intent.ACTION_SEND);
	   				
	   				email.setType("plain/text");
	   
	   	            email.putExtra(android.content.Intent.EXTRA_EMAIL, new String[]{"support@ifexx.com"});
	   	            email.putExtra(android.content.Intent.EXTRA_SUBJECT, "iFexx question");
	   	            email.putExtra(android.content.Intent.EXTRA_TEXT, 
	   	                    "name:"+name.getText().toString()+'\n'+"address:"+address.getText().toString()+'\n'+"message:"+message.getText().toString()+'\n'+'\n'+"Sent from the Lads to Leaders/Leaderettes Android App.");
	   
	   	            
	   	            startActivity(Intent.createChooser(email, "Send mail..."));
	   				
	   			}
	   		});


}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.message_page, menu);
		return true;
	}

}
