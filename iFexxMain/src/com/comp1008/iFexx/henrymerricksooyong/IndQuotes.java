package com.comp1008.iFexx.henrymerricksooyong;


import com.example.ifexxmain.R;

import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;

import android.view.Menu;
import android.view.View;

import android.webkit.WebView;

import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

public class IndQuotes extends Activity {
	ImageView img;
	WebView webview;
	String currency;
	String minuteChart, hourChart, dayChart, monthChart, open, high, low, last, changepip, changepercent;
	
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_ind_quotes);
		setupSignalslow();
		webview = (WebView)this.findViewById(R.id.marketChart);
		TextView currencyPair = (TextView)this.findViewById(R.id.lbl_currency);
		TextView openvalue = (TextView)this.findViewById(R.id.lbl_open);
		TextView highvalue = (TextView)this.findViewById(R.id.lbl_high);
		TextView lowvalue = (TextView)this.findViewById(R.id.lbl_low);
		TextView lastvalue = (TextView)this.findViewById(R.id.lbl_last);
		TextView changepipvalue = (TextView)this.findViewById(R.id.lbl_changepip);
		TextView changepercentvalue = (TextView)this.findViewById(R.id.lbl_changepercent);
		
		Bundle extras = getIntent().getExtras(); 
		currency = extras.getString("currency");
		minuteChart = extras.getString("minuteChart");
		hourChart = extras.getString("hourChart");
		dayChart = extras.getString("dayChart");
		monthChart = extras.getString("monthChart");
		open = extras.getString("open");
		high = extras.getString("high");
		low = extras.getString("low");
		last = extras.getString("last");
		changepip = extras.getString("changepip");
		changepercent = extras.getString("changepercent");
		
		
		 if (extras != null) {
	           
            currencyPair.setText(currency);
            openvalue.setText(open);
            highvalue.setText(high);
            lowvalue.setText(low);
            lastvalue.setText(last);
            changepipvalue.setText(changepip);
            changepercentvalue.setText(changepercent);
            String htmlString = "<!DOCTYPE html><html><body style = \"text-align:center\"><img src=\"http://forex.tradingcharts.com"+minuteChart+"\" alt=\"pageNo\" height=\"100%\" width=\"100%\"></body></html>";
        	webview.loadDataWithBaseURL(null,htmlString,"text/html","UTF-8","about:blank");
			}

		 Button hour = (Button) findViewById(R.id.hour);
		 Button minute = (Button) findViewById(R.id.min);
		 Button day = (Button) findViewById(R.id.day);
		 Button month = (Button) findViewById(R.id.mon);
		 
			minute.setOnClickListener(new View.OnClickListener() {
				
				@Override
				public void onClick(View v) {
					String htmlString = "<!DOCTYPE html><html><body style = \"text-align:center\"><img src=\"http://forex.tradingcharts.com"+minuteChart+"\" alt=\"pageNo\" height=\"100%\" width=\"100%\"></body></html>";
		        	webview.loadDataWithBaseURL(null,htmlString,"text/html","UTF-8","about:blank");
				}
					
				});
			
		hour.setOnClickListener(new View.OnClickListener() {
				
				@Override
				public void onClick(View v) {
					String htmlString = "<!DOCTYPE html><html><body style = \"text-align:center\"><img src=\"http://forex.tradingcharts.com"+hourChart+"\" alt=\"pageNo\" height=\"100%\" width=\"100%\"></body></html>";
		        	webview.loadDataWithBaseURL(null,htmlString,"text/html","UTF-8","about:blank");
				}
					
				});
		
		day.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				String htmlString = "<!DOCTYPE html><html><body style = \"text-align:center\"><img src=\"http://forex.tradingcharts.com"+dayChart+"\" alt=\"pageNo\" height=\"100%\" width=\"100%\"></body></html>";
	        	webview.loadDataWithBaseURL(null,htmlString,"text/html","UTF-8","about:blank");
			}
				
			});
		
		month.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				String htmlString = "<!DOCTYPE html><html><body style = \"text-align:center\"><img src=\"http://forex.tradingcharts.com"+monthChart+"\" alt=\"pageNo\" height=\"100%\" width=\"100%\"></body></html>";
	        	webview.loadDataWithBaseURL(null,htmlString,"text/html","UTF-8","about:blank");
			}
				
			});
		
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.ind_quotes, menu);
		return true;
	}
	private void setupSignalslow() {
		ImageButton lowsig = (ImageButton) findViewById(R.id.footersignalsbutton);
		
		lowsig.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				startActivity(new Intent(IndQuotes.this, AllSignals.class));
				
				
			}
		});
		
		setupMarketlow();
		
	}

	private void setupMarketlow() {
		ImageButton lowmarket = (ImageButton) findViewById(R.id.footermarketbutton);
		
		lowmarket.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				startActivity(new Intent(IndQuotes.this, LiveQuotes.class));
				
			}
		});
		
		setupNewslow();
		
	}

	private void setupNewslow() {
		ImageButton lownews = (ImageButton) findViewById(R.id.footernewsbutton);
		
		lownews.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				startActivity(new Intent(IndQuotes.this, AllNews.class));
				
			}
		});
		
		setupMenulow();
		
	}

	private void setupMenulow() {
		ImageButton lowmenu = (ImageButton) findViewById(R.id.footermenubutton);
		
		lowmenu.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				startActivity(new Intent(IndQuotes.this, Menupage.class));
				
			}
		});
		
		setupBacklow();
		
	}

	private void setupBacklow() {
		ImageButton lowback = (ImageButton) findViewById(R.id.footerbackbutton);
		
		lowback.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				finish();
				
			}
		});
	}
}
