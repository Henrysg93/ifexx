package com.comp1008.iFexx.henrymerricksooyong;

import com.example.ifexxmain.R;

import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.view.Menu;
import android.view.View;
import android.webkit.WebView;
import android.widget.ImageButton;

public class EconomicCalendar extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_economic_calendar);
		
setupSignalslow();
		
	}

private void setupSignalslow() {
		ImageButton lowsig = (ImageButton) findViewById(R.id.footersignalsbutton);
		
		lowsig.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				startActivity(new Intent(EconomicCalendar.this, AllSignals.class));
				
				
			}
		});
		
		setupMarketlow();
		
	}

	private void setupMarketlow() {
		ImageButton lowmarket = (ImageButton) findViewById(R.id.footermarketbutton);
		
		lowmarket.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				startActivity(new Intent(EconomicCalendar.this, LiveQuotes.class));
				
			}
		});
		
		setupNewslow();
		
	}

	private void setupNewslow() {
		ImageButton lownews = (ImageButton) findViewById(R.id.footernewsbutton);
		
		lownews.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				startActivity(new Intent(EconomicCalendar.this, AllNews.class));
				
			}
		});
		
		setupMenulow();
		
	}

	private void setupMenulow() {
		ImageButton lowmenu = (ImageButton) findViewById(R.id.footermenubutton);
		
		lowmenu.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				startActivity(new Intent(EconomicCalendar.this, Menupage.class));
				
			}
		});
		
		setupBacklow();
		
	}

	private void setupBacklow() {
		ImageButton lowback = (ImageButton) findViewById(R.id.footerbackbutton);
		
		lowback.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				finish();
				
			}
		});
		
		WebView webView = (WebView)findViewById(R.id.webView);
		
		// you can load an URL 
		webView.loadUrl("https://www.google.com/calendar/htmlembed?src=255ugs6rl7j89rttbo1k6ljki8%40group.calendar.google.com&ctz=Europe/London");
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.economic_calendar, menu);
		return true;
	}

}
