package com.comp1008.iFexx.henrymerricksooyong;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;

import com.example.ifexxmain.R;

import android.os.Bundle;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.text.Html;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;

public class IndSignal extends Activity {
	WebView webview;
	TextView txtweb;
	String html;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_ind_signal);
		Bundle b = getIntent().getExtras(); 
		String url = b.getString("url");
		
		  html = signalData(url);
		
		  final Context myApp = this;

		  /* An instance of this class will be registered as a JavaScript interface */
		  class MyJavaScriptInterface
		  {
		      @SuppressWarnings("unused")
		      public void processHTML(String html)
		      {
		          // process the html as needed by the app
		      }
		  }

		  final WebView browser = (WebView)findViewById(R.id.indSignal);
		  /* JavaScript must be enabled if you want it to work, obviously */
		  browser.getSettings().setJavaScriptEnabled(true);

		  /* Register a new JavaScript interface called HTMLOUT */
		  browser.addJavascriptInterface(new MyJavaScriptInterface(), "HTMLOUT");

		  /* WebViewClient must be set BEFORE calling loadUrl! */
		  browser.setWebViewClient(new WebViewClient() {
		      @Override
		      public void onPageFinished(WebView view, String url)
		      {
		          /* This call inject JavaScript into the page which just finished loading. */
		          browser.loadUrl("javascript:window.HTMLOUT.processHTML('<head>'+document.getElementsByTagName('html')[0].innerHTML+'</head>');");
		      }
		  });

		  /* load a web page */
		  browser.loadUrl(url);

		
	
	
	setupSignalslow();
}
	
	public String signalData(String url) {
        // Create a new HttpClient and Post Header
        HttpClient httpclient = new DefaultHttpClient();
         
        /* login.php returns true if username and password is correct */
        HttpPost httppost = new HttpPost(url);
        String str = null;
        try {
      
         
 
            // Execute HTTP Post Request
            Log.w("IFEXX", "Execute HTTP Post Request");
            HttpResponse response = httpclient.execute(httppost);
             
            str = inputStreamToString(response.getEntity().getContent()).toString();
           
           
             
    
 
        } catch (ClientProtocolException e) {
         e.printStackTrace();
        } catch (IOException e) {
         e.printStackTrace();
        }

		return str;
    } 
	private StringBuilder inputStreamToString(InputStream is) {
	     String line = "";
	     StringBuilder total = new StringBuilder();
	     // Wrap a BufferedReader around the InputStream
	     BufferedReader rd = new BufferedReader(new InputStreamReader(is));
	     // Read response until the end
	     try {
	      while ((line = rd.readLine()) != null) { 
	        total.append(line); 
	      }
	     } catch (IOException e) {
	      e.printStackTrace();
	     }
	     // Return full string
	     return total;
	    }


private void setupSignalslow() {
	ImageButton lowsig = (ImageButton) findViewById(R.id.footersignalsbutton);
	
	lowsig.setOnClickListener(new View.OnClickListener() {
		
		@Override
		public void onClick(View v) {
			startActivity(new Intent(IndSignal.this, AllSignals.class));
			
			
		}
	});
	
	setupMarketlow();
	
}

private void setupMarketlow() {
	ImageButton lowmarket = (ImageButton) findViewById(R.id.footermarketbutton);
	
	lowmarket.setOnClickListener(new View.OnClickListener() {
		
		@Override
		public void onClick(View v) {
			startActivity(new Intent(IndSignal.this, LiveQuotes.class));
			
		}
	});
	
	setupNewslow();
	
}

private void setupNewslow() {
	ImageButton lownews = (ImageButton) findViewById(R.id.footernewsbutton);
	
	lownews.setOnClickListener(new View.OnClickListener() {
		
		@Override
		public void onClick(View v) {
			startActivity(new Intent(IndSignal.this, AllNews.class));
			
		}
	});
	
	setupMenulow();
	
}

private void setupMenulow() {
	ImageButton lowmenu = (ImageButton) findViewById(R.id.footermenubutton);
	
	lowmenu.setOnClickListener(new View.OnClickListener() {
		
		@Override
		public void onClick(View v) {
			startActivity(new Intent(IndSignal.this, Menupage.class));
			
		}
	});
	
	setupBacklow();
	
}

private void setupBacklow() {
	ImageButton lowback = (ImageButton) findViewById(R.id.footerbackbutton);
	
	lowback.setOnClickListener(new View.OnClickListener() {
		
		@Override
		public void onClick(View v) {
			finish();
			
		}
	});
}
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.ind_signal, menu);
		return true;
	}

}
