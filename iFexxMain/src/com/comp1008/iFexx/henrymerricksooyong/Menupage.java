package com.comp1008.iFexx.henrymerricksooyong;

import com.example.ifexxmain.R;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.Calendar;
import java.util.Date;
import java.util.Random;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.content.ContentResolver;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.EditText;

import com.mixpanel.android.mpmetrics.MixpanelAPI;


import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.view.Menu;
import android.view.View;
import android.widget.ImageButton;

public class Menupage extends Activity {
	
	public static final String MIXPANEL_API_TOKEN = "fe6cc4fc87362d46c3dceabb165b74b2";
	
	  public static final String ANDROID_PUSH_SENDER_ID = "245876667774";
	  
	  @Override
	    protected void onResume() {
	        super.onResume();

	        final long nowInHours = hoursSinceEpoch();
	        final int hourOfTheDay = hourOfTheDay();

	        // For our simple test app, we're interested tracking
	        // when the user views our application.

	        // It will be interesting to segment our data by the date that they
	        // first viewed our app. We use a
	        // superProperty (so the value will always be sent with the
	        // remainder of our events) and register it with
	        // registerSuperPropertiesOnce (so no matter how many times
	        // the code below is run, the events will always be sent
	        // with the value of the first ever call for this user.)
	        // all the change we make below are LOCAL. No API requests are made.
	        try {
	            final JSONObject properties = new JSONObject();
	            properties.put("first viewed on", nowInHours);
	            properties.put("user domain", "(unknown)"); // default value
	            mMixpanel.registerSuperPropertiesOnce(properties);
	        } catch (final JSONException e) {
	            throw new RuntimeException("Could not encode hour first viewed as JSON");
	        }

	        // Now we send an event to Mixpanel. We want to send a new
	        // "App Resumed" event every time we are resumed, and
	        // we want to send a current value of "hour of the day" for every event.
	        // As usual,all of the user's super properties will be appended onto this event.
	        try {
	            final JSONObject properties = new JSONObject();
	            properties.put("hour of the day", hourOfTheDay);
	            mMixpanel.track("App Resumed", properties);
	        } catch(final JSONException e) {
	            throw new RuntimeException("Could not encode hour of the day in JSON");
	        }
	    }

	    

	    // This is an example of how you can use Mixpanel's revenue tracking features from Android.
	    public void recordRevenue(final View view) {
	        final MixpanelAPI.People people = mMixpanel.getPeople();
	        // Call trackCharge() with a floating point amount
	        // (for example, the amount of money the user has just spent on a purchase)
	        // and an optional set of properties describing the purchase.
	        people.trackCharge(1.50, null);
	    }

	    @Override
	    protected void onDestroy() {
	        super.onDestroy();

	        // To preserve battery life, the Mixpanel library will store
	        // events rather than send them immediately. This means it
	        // is important to call flush() to send any unsent events
	        // before your application is taken out of memory.
	        mMixpanel.flush();
	    }

	    ////////////////////////////////////////////////////

	    public void setBackgroundImage(final View view) {
	        final Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
	        photoPickerIntent.setType("image/*");
	        startActivityForResult(photoPickerIntent, PHOTO_WAS_PICKED);
	    }

	    @Override
	    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
	        if (PHOTO_WAS_PICKED == requestCode && null != data) {
	            final Uri imageUri = data.getData();
	            if (null != imageUri) {
	                // AsyncTask, please...
	                final ContentResolver contentResolver = getContentResolver();
	                try {
	                    final InputStream imageStream = contentResolver.openInputStream(imageUri);
	                    System.out.println("DRAWING IMAGE FROM URI " + imageUri);
	                    final Bitmap background = BitmapFactory.decodeStream(imageStream);
	                    getWindow().setBackgroundDrawable(new BitmapDrawable(getResources(), background));
	                } catch (final FileNotFoundException e) {
	                    Log.e(LOGTAG, "Image apparently has gone away", e);
	                }
	            }
	        }
	    }

	    private String getTrackingDistinctId() {
	        final SharedPreferences prefs = getPreferences(MODE_PRIVATE);

	        String ret = prefs.getString(MIXPANEL_DISTINCT_ID_NAME, null);
	        if (ret == null) {
	            ret = generateDistinctId();
	            final SharedPreferences.Editor prefsEditor = prefs.edit();
	            prefsEditor.putString(MIXPANEL_DISTINCT_ID_NAME, ret);
	            prefsEditor.commit();
	        }

	        return ret;
	    }

	    // These disinct ids are here for the purposes of illustration.
	    // In practice, there are great advantages to using distinct ids that
	    // are easily associated with user identity, either from server-side
	    // sources, or user logins. A common best practice is to maintain a field
	    // in your users table to store mixpanel distinct_id, so it is easily
	    // accesible for use in attributing cross platform or server side events.
	    private String generateDistinctId() {
	        final Random random = new Random();
	        final byte[] randomBytes = new byte[32];
	        random.nextBytes(randomBytes);
	        return Base64.encodeToString(randomBytes, Base64.NO_WRAP | Base64.NO_PADDING);
	    }

	    ///////////////////////////////////////////////////////
	    // conveniences

	    private int hourOfTheDay() {
	        final Calendar calendar = Calendar.getInstance();
	        return calendar.get(Calendar.HOUR_OF_DAY);
	    }

	    private long hoursSinceEpoch() {
	        final Date now = new Date();
	        final long nowMillis = now.getTime();
	        return nowMillis / 1000 * 60 * 60;
	    }

	    private String domainFromEmailAddress(String email) {
	        String ret = "";
	        final int atSymbolIndex = email.indexOf('@');
	        if ((atSymbolIndex > -1) && (email.length() > atSymbolIndex)) {
	            ret = email.substring(atSymbolIndex + 1);
	        }

	        return ret;
	    }

	    private MixpanelAPI mMixpanel;
	    private static final String MIXPANEL_DISTINCT_ID_NAME = "Mixpanel Example $distinctid";
	    private static final int PHOTO_WAS_PICKED = 2;
	    private static final String LOGTAG = "Mixpanel Example Application";
	






	@Override
	protected void onCreate(Bundle savedInstanceState) {
		
		
		super.onCreate(savedInstanceState);
        final String trackingDistinctId = getTrackingDistinctId();

        // Initialize the Mixpanel library for tracking and push notifications.
        mMixpanel = MixpanelAPI.getInstance(this, MIXPANEL_API_TOKEN);


        // We also identify the current user with a distinct ID, and
        // register ourselves for push notifications from Mixpanel.

        mMixpanel.identify(trackingDistinctId); //this is the distinct_id value that
        // will be sent with events. If you choose not to set this,
        // the SDK will generate one for you

        mMixpanel.getPeople().identify(trackingDistinctId); //this is the distinct_id
        // that will be used for people analytics. You must set this explicitly in order
        // to dispatch people data.

        // People analytics must be identified separately from event analytics.
        // The data-sets are separate, and may have different unique keys (distinct_id).
        // We recommend using the same distinct_id value for a given user in both,
        // and identifying the user with that id as early as possible.

        mMixpanel.getPeople().initPushHandling(ANDROID_PUSH_SENDER_ID);
        
		setContentView(R.layout.activity_menupage);
		
		setupSignalsbutton();
	}

	private void setupSignalsbutton() {
		
		ImageButton signals = (ImageButton) findViewById(R.id.mainsignalsbutton);
		
		signals.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				startActivity(new Intent(Menupage.this, AllSignals.class));
				
			}
		});
		
		
		setupNewsbutton();
	}

	private void setupNewsbutton() {
		ImageButton news = (ImageButton) findViewById(R.id.mainnewsbutton);
		
		news.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				startActivity(new Intent(Menupage.this, AllNews.class));
				
			}
		});
		
		setupCalendar();
		
	}

	private void setupCalendar() {
		ImageButton calendar = (ImageButton) findViewById(R.id.maincalendarbutton);
		
		calendar.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				startActivity(new Intent(Menupage.this, EconomicCalendar.class));
				
			}
		});
		
		setupMessagebutton();
		
	}

	private void setupMessagebutton() {
		ImageButton message = (ImageButton) findViewById(R.id.mainmessagesbutton);
		
		message.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				startActivity(new Intent(Menupage.this, MessagePage.class));
				
			}
		});
		
		setupQuotesbutton();
		
	}

	private void setupQuotesbutton() {
		ImageButton quotes = (ImageButton) findViewById(R.id.mainmarketbutton);
		
		quotes.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				startActivity(new Intent(Menupage.this, LiveQuotes.class));
				
			}
		});
		
		setupSettingsbutton();
		
	}

	private void setupSettingsbutton() {
		ImageButton settings = (ImageButton) findViewById(R.id.messagesendbutton);
		
		settings.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				startActivity(new Intent(Menupage.this, Settingspage.class));
				
			}
		});
		
		
		setupLogoutbutton();
	}

	private void setupLogoutbutton() {
		ImageButton logout = (ImageButton) findViewById(R.id.mainlogoutbutton);
		
		logout.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				startActivity(new Intent(Menupage.this, LoginPage.class));
				
			}
			
		});
				
				setupSignalslow();
				
			}

	private void setupSignalslow() {
				ImageButton lowsig = (ImageButton) findViewById(R.id.footersignalsbutton);
				
				lowsig.setOnClickListener(new View.OnClickListener() {
					
					@Override
					public void onClick(View v) {
						startActivity(new Intent(Menupage.this, AllSignals.class));
						
						
					}
				});
				
				setupMarketlow();
				
			}

			private void setupMarketlow() {
				ImageButton lowmarket = (ImageButton) findViewById(R.id.footermarketbutton);
				
				lowmarket.setOnClickListener(new View.OnClickListener() {
					
					@Override
					public void onClick(View v) {
						startActivity(new Intent(Menupage.this, LiveQuotes.class));
						
					}
				});
				
				setupNewslow();
				
			}

			private void setupNewslow() {
				ImageButton lownews = (ImageButton) findViewById(R.id.footernewsbutton);
				
				lownews.setOnClickListener(new View.OnClickListener() {
					
					@Override
					public void onClick(View v) {
						startActivity(new Intent(Menupage.this, AllNews.class));
						
					}
				});
				
				setupMenulow();
				
			}

			private void setupMenulow() {
				ImageButton lowmenu = (ImageButton) findViewById(R.id.footermenubutton);
				
				lowmenu.setOnClickListener(new View.OnClickListener() {
					
					@Override
					public void onClick(View v) {
						startActivity(new Intent(Menupage.this, Menupage.class));
						
					}
				});
				
				setupBacklow();
				
			}

			private void setupBacklow() {
				ImageButton lowback = (ImageButton) findViewById(R.id.footerbackbutton);
				
				lowback.setOnClickListener(new View.OnClickListener() {
					
					@Override
					public void onClick(View v) {
						finish();
						
					}
				});
				
			

			}
	

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.menupage, menu);
		return true;
	}
	
	}

	