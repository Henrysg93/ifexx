package com.comp1008.iFexx.henrymerricksooyong;

import com.comp1008.iFexx.henrymerricksooyong.data.RssItem;
import com.comp1008.iFexx.henrymerricksooyong.listeners.ListListener;
import com.comp1008.iFexx.henrymerricksooyong.util.RssReader;
import com.example.ifexxmain.R;


import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.ListView;

public class AllNews extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_all_news);
		
		try {
			// Create RSS reader
			RssReader rssReader = new RssReader("http://www.feedyes.com/feed.php?f=t0xzXT0qqFe20SE9");
			// Get a ListView from main view
			ListView itcItems = (ListView) findViewById(R.id.listView1);
			
			// Create a list adapter
			ArrayAdapter<RssItem> adapter = new ArrayAdapter<RssItem>(this,android.R.layout.simple_list_item_1, rssReader.getItems());
			// Set list adapter for the ListView
			itcItems.setAdapter(adapter);
			
			// Set list view item click listener
			itcItems.setOnItemClickListener((OnItemClickListener) new ListListener(rssReader.getItems(), this));
			
		} catch (Exception e) {
			Log.e("ITCRssReader", e.getMessage());
		}
		
		setupSignalslow();
		
	}

private void setupSignalslow() {
		ImageButton lowsig = (ImageButton) findViewById(R.id.footersignalsbutton);
		
		lowsig.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				startActivity(new Intent(AllNews.this, AllSignals.class));
				
				
			}
		});
		
		setupMarketlow();
		
	}

	private void setupMarketlow() {
		ImageButton lowmarket = (ImageButton) findViewById(R.id.footermarketbutton);
		
		lowmarket.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				startActivity(new Intent(AllNews.this, LiveQuotes.class));
				
			}
		});
		
		setupNewslow();
		
	}

	private void setupNewslow() {
		ImageButton lownews = (ImageButton) findViewById(R.id.footernewsbutton);
		
		lownews.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				startActivity(new Intent(AllNews.this, AllNews.class));
				
			}
		});
		
		setupMenulow();
		
	}

	private void setupMenulow() {
		ImageButton lowmenu = (ImageButton) findViewById(R.id.footermenubutton);
		
		lowmenu.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				startActivity(new Intent(AllNews.this, Menupage.class));
				
			}
		});
		
		setupBacklow();
		
	}

	private void setupBacklow() {
		ImageButton lowback = (ImageButton) findViewById(R.id.footerbackbutton);
		
		lowback.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				finish();
				
			}
		});
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.all_news, menu);
		return true;
	}

}
