package com.comp1008.iFexx.henrymerricksooyong;

import com.example.ifexxmain.R;

import android.os.Bundle;
import android.os.Handler;
import android.app.Activity;
import android.content.Intent;
import android.view.Menu;


public class Intro extends Activity {

	protected boolean _active = true;
	protected int _splashTime = 4000;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_intro);
		
		Handler handler = new Handler();
	    handler.postDelayed(new Runnable() {
	        public void run() {
	            finish();
	            Intent i3 = new Intent(Intro.this, LoginPage.class);
	                startActivity(i3);
	        }
	    }, _splashTime);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.intro, menu);
		return true;
	}

}
