package com.comp1008.iFexx.henrymerricksooyong;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import com.example.ifexxmain.R;

import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.app.Activity;

import android.content.Intent;
import android.view.Menu;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.ListView;


public class LiveQuotes extends Activity {
	ListView lv;
	String url = "http://www.myfxbook.com/forex-market/currencies";

	
	//  Listview items
	String[] currencies = new String[] {
			"AUDCAD",
			"AUDJPY",
			"AUDNZD",
			"AUDUSD",
			"CADJPY",
			"EURAUD",
			"EURCAD",
			"EURCHF",
			"EURCZK",
			"EURGBP",
			"EURJPY",
			"EURUSD",
			"GBPJPY",
			"GBPUSD",
			"NZDUSD",
			"USDCAD",
			"USDCHF",
			"USDJPY",
			"XAGUSD",
			"XAUUSD"
			
	};

	

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_live_quotes);
		lv= (ListView) findViewById(R.id.listView1);
		

		
		ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, currencies);
	     lv.setAdapter(adapter);
		
		lv.setOnItemClickListener(new OnItemClickListener() {
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

				// we use the items of the listview as title of the next activity
				 String currency = currencies[position];

	
					String[] minuteChart = getResources().getStringArray(R.array.minuteChart);
			        final String minuteChartSource = minuteChart[position];
				 
			        String[] hourChart = getResources().getStringArray(R.array.hourChart);
			        final String hourChartSource = hourChart[position];
			        
			        String[] dayChart = getResources().getStringArray(R.array.dayChart);
			        final String dayChartSource = dayChart[position];
			        
			        String[] monthChart = getResources().getStringArray(R.array.monthChart);
			        final String monthChartSource = monthChart[position];

			        ArrayList<String>open = new ArrayList<String>();
					 open = getOpen();
					 ArrayList<String>low = new ArrayList<String>();
					 low = getLow();
					 ArrayList<String>high = new ArrayList<String>();
					 high = getHigh();
					 ArrayList<String>last = new ArrayList<String>();
					 last = getLast();
					 ArrayList<String>changepip = new ArrayList<String>();
					 changepip = getChangePips();
					 ArrayList<String>changepercent = new ArrayList<String>();
					 changepercent = getChangePercent();
					 
					 
			        String openvalue = open.get(position);
			        String highvalue = high.get(position);
			        String lowvalue = low.get(position);
			        String lastvalue = last.get(position);
			        String changepipsvalue = changepip.get(position);
			        String changepercentvalue = changepercent.get(position);
			        
				Intent intent = new Intent(getApplicationContext(), IndQuotes.class);
				intent.putExtra("currency", currency);
				intent.putExtra("minuteChart", minuteChartSource);
				intent.putExtra("hourChart", hourChartSource);
				intent.putExtra("dayChart", dayChartSource);
				intent.putExtra("monthChart", monthChartSource);
				intent.putExtra("open", openvalue);
				intent.putExtra("high", highvalue);
				intent.putExtra("low", lowvalue);
				intent.putExtra("last", lastvalue);
				intent.putExtra("changepip", changepipsvalue);
				intent.putExtra("changepercent", changepercentvalue);

				startActivity(intent);      
				
			}
		});
				
        callAsynchronousTask();
        setupSignalslow();
		
	}

	public void callAsynchronousTask() {
	    final Handler handler = new Handler();
	    Timer timer = new Timer();
	    TimerTask doAsynchronousTask = new TimerTask() {       
	        @Override
	        public void run() {
	            handler.post(new Runnable() {
	                public void run() {       
	                    try {
	                        MarketQuote performBackgroundTask = new MarketQuote();
	                        // PerformBackgroundTask this class is the class that extends AsynchTask 
	                        performBackgroundTask.execute();

	                    } catch (Exception e) {
	                        // TODO Auto-generated catch block
	                    }
	                }
	            });
	        }
	    };
	    timer.schedule(doAsynchronousTask, 0, 5000); //execute in every 50000 ms
	}
	
private void setupSignalslow() {
		ImageButton lowsig = (ImageButton) findViewById(R.id.footersignalsbutton);
		
		lowsig.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				startActivity(new Intent(LiveQuotes.this, AllSignals.class));
				
				
			}
		});
		
		setupMarketlow();
		
	}

	private void setupMarketlow() {
		ImageButton lowmarket = (ImageButton) findViewById(R.id.footermarketbutton);
		
		lowmarket.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				startActivity(new Intent(LiveQuotes.this, LiveQuotes.class));
				
			}
		});
		
		setupNewslow();
		
	}

	private void setupNewslow() {
		ImageButton lownews = (ImageButton) findViewById(R.id.footernewsbutton);
		
		lownews.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				startActivity(new Intent(LiveQuotes.this, AllNews.class));
				
			}
		});
		
		setupMenulow();
		
	}

	private void setupMenulow() {
		ImageButton lowmenu = (ImageButton) findViewById(R.id.footermenubutton);
		
		lowmenu.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				startActivity(new Intent(LiveQuotes.this, Menupage.class));
				
			}
		});
		
		setupBacklow();
		
	}

	private void setupBacklow() {
		ImageButton lowback = (ImageButton) findViewById(R.id.footerbackbutton);
		
		lowback.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				finish();
				
			}
		});
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.live_quotes, menu);
		return true;
	}
	
	
	 private class MarketQuote extends AsyncTask<String, Void, ArrayList<String>> {
  	   ArrayList<String> temparrlist = new ArrayList<String>();
  	    protected void onPreExecute(){
  
  	    }

  	    protected ArrayList<String> doInBackground(String... connection) {
  	              temparrlist=getMarketTable();
  	            return temparrlist;
  	    }

  	    protected void onPostExecute(ArrayList<String> result) {
  	       final ArrayAdapter<String> arrayAdapter = 
  	                new ArrayAdapter<String>(LiveQuotes.this,
  	                              android.R.layout.simple_list_item_1, 
  	                              result);
  	       lv.setAdapter(arrayAdapter);  
  	}
  
  
  	    
  public ArrayList<String> getMarketTable(){
  	ArrayList<String> markettable = new ArrayList<String>();
  	
  	try {
          // Connect to the web site
          Document doc = Jsoup.connect(url).get();

          Element containingDiv = doc.select("#symbolMarket").first();
          Elements table = containingDiv.select("table");
          Elements rows = table.select("tr");
          

          for (Element row : rows) {
              markettable.add(row.child(0).text()+ "         " +row.child(4).text()+"          "+row.child(5).text());
              
          }
      } catch (IOException e) {
          e.printStackTrace();
      }
  	markettable.remove(0);
  	return markettable;
  	}
}
	 
	

public ArrayList<String> getLow(){
	ArrayList<String> low = new ArrayList<String>();
	
	try {
        // Connect to the web site
        Document doc = Jsoup.connect(url).get();

        Element containingDiv = doc.select("#symbolMarket").first();
        Elements table = containingDiv.select("table");
        Elements rows = table.select("tr");
        

        for (Element row : rows) {
            low.add("LOW : "+row.child(3).text());
            
        }
    } catch (IOException e) {
        e.printStackTrace();
    }
	low.remove(0);
	return low;
	}

public ArrayList<String> getLast(){
	ArrayList<String> last = new ArrayList<String>();
	
	try {
        // Connect to the web site
        Document doc = Jsoup.connect(url).get();

        Element containingDiv = doc.select("#symbolMarket").first();
        Elements table = containingDiv.select("table");
        Elements rows = table.select("tr");
        

        for (Element row : rows) {
            last.add("LAST : "+row.child(4).text());
            
        }
    } catch (IOException e) {
        e.printStackTrace();
    }
	last.remove(0);
	return last;
	}

public ArrayList<String> getChangePips(){
	ArrayList<String> changepips = new ArrayList<String>();
	
	try {
        // Connect to the web site
        Document doc = Jsoup.connect(url).get();

        Element containingDiv = doc.select("#symbolMarket").first();
        Elements table = containingDiv.select("table");
        Elements rows = table.select("tr");
        

        for (Element row : rows) {
            changepips.add("CHANGE (Pips) : "+row.child(5).text());
            
        }
    } catch (IOException e) {
        e.printStackTrace();
    }
	changepips.remove(0);
	return changepips;
	}

public ArrayList<String> getChangePercent(){
	ArrayList<String> changepercent = new ArrayList<String>();
	
	try {
        // Connect to the web site
        Document doc = Jsoup.connect(url).get();

        Element containingDiv = doc.select("#symbolMarket").first();
        Elements table = containingDiv.select("table");
        Elements rows = table.select("tr");
        

        for (Element row : rows) {
            changepercent.add("CHANGE(%) : "+row.child(6).text());
            
        }
    } catch (IOException e) {
        e.printStackTrace();
    }
	changepercent.remove(0);
	return changepercent;
	}


public ArrayList<String>getOpen(){
  	ArrayList<String> open = new ArrayList<String>();
  	
  	try {
          // Connect to the web site
          Document doc = Jsoup.connect(url).get();

          Element containingDiv = doc.select("#symbolMarket").first();
          Elements table = containingDiv.select("table");
          Elements rows = table.select("tr");
          

          for (Element row : rows) {
              open.add("OPEN : "+row.child(1).text());
              
          }
      } catch (IOException e) {
          e.printStackTrace();
      }
  	open.remove(0);
  	return open;
  	}





	    
		 public ArrayList<String> getHigh(){
			  	ArrayList<String> high = new ArrayList<String>();
			  	
			  	try {
			          // Connect to the web site
			          Document doc = Jsoup.connect(url).get();

			          Element containingDiv = doc.select("#symbolMarket").first();
			          Elements table = containingDiv.select("table");
			          Elements rows = table.select("tr");
			          

			          for (Element row : rows) {
			              high.add("HIGH : "+row.child(2).text());
			              
			          }
			      } catch (IOException e) {
			          e.printStackTrace();
			      }
			  	high.remove(0);
			  	return high;
			  	}

	
}
	 
